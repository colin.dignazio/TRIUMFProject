﻿namespace TRIUMF
{
    class FiscalDate
    {
        private readonly int regY;
        private readonly int regM;

        public FiscalDate(int y, int m)
        {
            regY = y;
            regM = m;
        }

        public int GetFiscalYear()
        {
            if (regM > 3)
                return regY;
            else
                return regY - 1;
        }

        public int GetFiscalMonth()
        {
            if (regM > 3)
                return regM - 3;
            else
                return regM + 9;
        }
    }
}
