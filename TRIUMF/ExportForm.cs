﻿using System;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace TRIUMF
{
    public partial class ExportForm : Form
    {
        private ExcelExport xlExport;
        private Excel.Application xlApp;

        public ExportForm(string exportMethod)
        {
            xlExport = new ExcelExport(exportMethod);
            InitializeComponent();
        }

        private void ExportForm_Load(object sender, EventArgs e)
        {
            string fileName;

            try
            {
                xlApp = (Excel.Application)Marshal.GetActiveObject("Excel.Application");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                fileName = getExcelFile();
                xlApp = new Excel.Application();
                xlApp.Workbooks.Open(@fileName);
                this.BringToFront();
            }

            if (xlApp.ActiveWorkbook == null)
            {
                openExcelFile();
            }

            if (xlApp.ActiveWorkbook.Worksheets.Count == 0)
            {
                xlApp.ActiveWorkbook.Worksheets.Add();
            }

            updateComboList();
            exportTextBox.Text = "Enter an identifier and select a worksheet:";
            exportInput.Text = Globals.ThisAddIn.Application.ActiveProject.Name;
        }  

        private void updateComboList()
        {
            exportCombo.Items.Clear();

            foreach (Excel.Worksheet workSheet in xlApp.ActiveWorkbook.Worksheets)
            {
                exportCombo.Items.Add(xlApp.ActiveWorkbook.Name + " - " + workSheet.Name);
            }

            exportCombo.SelectedIndex = 0;
        }
        
        private string getExcelFile()
        {
            string result = null;
            OpenFileDialog fileDialog = new OpenFileDialog();

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                result = fileDialog.FileName;
            }

            return result;
        }   
        
        private void openExcelFile()
        {
            string fileName = getExcelFile();

            try
            {
                xlApp.Workbooks.Open(@fileName);
                this.BringToFront();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                this.Close();
            }
        }    

        public void submit_Click(object sender, EventArgs e)
        {
            export();
        }

        private void instanceInput_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                export();
        }

        private void export()
        {
            string instanceName = exportInput.Text;
            string workSheetName = exportCombo.SelectedItem.ToString().Split(new char[] { '-' })[1].Trim();
            this.Close();
            xlExport.export(instanceName, workSheetName);
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void browse_Click(object sender, EventArgs e)
        {
            openExcelFile();
            updateComboList();
        }
    }
}
