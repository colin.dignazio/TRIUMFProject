﻿namespace TRIUMF
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.setDefaults = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.settingsTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // setDefaults
            // 
            this.setDefaults.Location = new System.Drawing.Point(64, 68);
            this.setDefaults.Name = "setDefaults";
            this.setDefaults.Size = new System.Drawing.Size(75, 23);
            this.setDefaults.TabIndex = 0;
            this.setDefaults.Text = "Set Defaults";
            this.setDefaults.UseVisualStyleBackColor = true;
            this.setDefaults.Click += new System.EventHandler(this.setDefaults_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(145, 68);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 1;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.close_Click);
            // 
            // settingsTextBox
            // 
            this.settingsTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.settingsTextBox.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.settingsTextBox.Location = new System.Drawing.Point(12, 12);
            this.settingsTextBox.Name = "settingsTextBox";
            this.settingsTextBox.ReadOnly = true;
            this.settingsTextBox.Size = new System.Drawing.Size(260, 50);
            this.settingsTextBox.TabIndex = 3;
            this.settingsTextBox.Text = "";
            // 
            // SettingsForm
            // 
            this.AcceptButton = this.setDefaults;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 103);
            this.Controls.Add(this.settingsTextBox);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.setDefaults);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.Text = "TRIUMF Settings";
            this.Load += new System.EventHandler(this.SettingsForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button setDefaults;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.RichTextBox settingsTextBox;
    }
}