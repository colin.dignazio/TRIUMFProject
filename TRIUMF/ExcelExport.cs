﻿using System;
using System.Runtime.InteropServices;
using MSProject = Microsoft.Office.Interop.MSProject;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows.Forms;
using System.Reflection;

namespace TRIUMF
{
    public class ExcelExport
    {
        private readonly string EXPORT_TYPE_NAME = "_EXPORT_TYPE_";
        private readonly string EXPORT_SCURVE_DATA = "_EXPORT_SCURVE_DATA_";
        private readonly string EXPORT_TIME_PHASED_WORK = "_EXPORT_TIME_PHASED_WORK_";
        private const string ERROR_MESG = "An error occured during the export.";
        private string exportMethod;

        private Excel.Application xlApp;
        private Excel.Workbook xlWorkbook;
        private Excel.Worksheet xlWorksheet;
        private Excel.Range xlRange;

        public ExcelExport(string exportMethod)
        {
            this.exportMethod = exportMethod;
        }

        public void export(string instanceName, string worksheetName)
        {
            Type thisType = this.GetType();
            MethodInfo theMethod = thisType.GetMethod(exportMethod);
            theMethod.Invoke(this, new object[] { instanceName, worksheetName });
        }

        public void XlExportSCurveData(string instanceName, string worksheetName)
        {
            // Remy Dawson 2015
            // Extracts monthly time phased costs from the active project and writes it to a spreadsheet.
            // The active worksheet in the active workbook receives the data.
            // Data from additional projects will be appended.

            long row;
            MSProject.Project activeProj;
            MSProject.TimeScaleValues tsvs; // cost
            MSProject.TimeScaleValues tsvsa; // actual cost
            MSProject.TimeScaleValues tsvsb, tsvsb1, tsvsb2, tsvsb3; // baseline costs
            double pc = 0;
            double pcc = 0;
            double pca = 0;
            double pcca = 0;
            double pcb = 0;
            double pccb = 0;
            double pcb1 = 0;
            double pccb1 = 0;
            double pcb2 = 0;
            double pccb2 = 0;
            double pcb3 = 0;
            double pccb3 = 0;
            int startYear, fisYear, startMonth, fisMonth;
            string startMonthStr;
            FiscalDate fisDate;

            initializeExcelEnv(worksheetName);

            try
            {
                // extracts data from the active project
                activeProj = Globals.ThisAddIn.Application.ActiveProject;

                // if there isn't anything that looks like a header row, add it
                //if(getExportType(xlWorksheet) != EXPORT_SCURVE_DATA)
                if(xlWorksheet.Cells[1, 1].Value != "File Name")
                {
                    //setExportType(xlWorksheet, EXPORT_SCURVE_DATA);
                    xlWorksheet.Rows[1].Insert();
                    xlWorksheet.Cells[1, 1].Value = "File Name";
                    xlWorksheet.Cells[1, 2].Value = "Year";
                    xlWorksheet.Cells[1, 3].Value = "Month";
                    xlWorksheet.Cells[1, 4].Value = "Fiscal Year";
                    xlWorksheet.Cells[1, 5].Value = "Fiscal Month";
                    xlWorksheet.Cells[1, 6].Value = "Cost";
                    xlWorksheet.Cells[1, 7].Value = "Cumulative";
                    xlWorksheet.Cells[1, 8].Value = "Actual";
                    xlWorksheet.Cells[1, 9].Value = "Actual Cumulative";
                    xlWorksheet.Cells[1, 10].Value = "Baseline";
                    xlWorksheet.Cells[1, 11].Value = "Baseline Cumulative";
                    xlWorksheet.Cells[1, 12].Value = "Baseline1";
                    xlWorksheet.Cells[1, 13].Value = "Baseline1 Cumulative";
                    xlWorksheet.Cells[1, 14].Value = "Baseline2";
                    xlWorksheet.Cells[1, 15].Value = "Baseline2 Cumulative";
                    xlWorksheet.Cells[1, 16].Value = "Baseline3";
                    xlWorksheet.Cells[1, 17].Value = "Baseline3 Cumulative";
                    row = 2;
                }
                else
                {
                    row = xlWorksheet.UsedRange.Rows.Count + 1;
                }

                // get the cost time scaled values for the active project
                tsvs = activeProj.ProjectSummaryTask.TimeScaleData(
                    StartDate: activeProj.ProjectSummaryTask.Start, EndDate: activeProj.ProjectSummaryTask.Finish,
                    Type: MSProject.PjTaskTimescaledData.pjTaskTimescaledCost,
                    TimeScaleUnit: MSProject.PjTimescaleUnit.pjTimescaleMonths, Count: 1);

                // Iterate the TimeScaledValues.
                foreach (MSProject.TimeScaleValue tsv in tsvs)
                {
                    if (!string.IsNullOrEmpty(tsv.Value.ToString()) && Convert.ToInt64(tsv.Value) != 0)
                    {
                        startYear = Convert.ToInt32(String.Format("{0:yyyy}", tsv.StartDate));
                        startMonthStr = String.Format("{0:MMM}", tsv.StartDate);
                        string month = String.Format("{0:MM}", tsv.StartDate);
                        startMonth = Convert.ToInt32(month);

                        fisDate = new FiscalDate(startYear, startMonth);
                        fisYear = fisDate.GetFiscalYear();
                        fisMonth = fisDate.GetFiscalMonth();

                        pcc = pcc + tsv.Value;
                        pc = tsv.Value;

                        // actual costs
                        tsvsa = activeProj.ProjectSummaryTask.TimeScaleData(
                            StartDate: tsv.StartDate,
                            EndDate: tsv.EndDate,
                            Type: MSProject.PjTaskTimescaledData.pjTaskTimescaledActualCost,
                            TimeScaleUnit: MSProject.PjTimescaleUnit.pjTimescaleMonths, Count: 1);

                        if (!string.IsNullOrEmpty(tsvsa[1].Value.ToString()) && Convert.ToInt64(tsvsa[1].Value) != 0)
                        {
                            pcca = pcca + tsvsa[1].Value;
                            pca = tsvsa[1].Value;
                        }

                        // baseline costs
                        tsvsb = activeProj.ProjectSummaryTask.TimeScaleData(
                            StartDate: tsv.StartDate,
                            EndDate: tsv.EndDate,
                            Type: MSProject.PjTaskTimescaledData.pjTaskTimescaledBaselineCost,
                            TimeScaleUnit: MSProject.PjTimescaleUnit.pjTimescaleMonths, Count: 1);

                        if (!string.IsNullOrEmpty(tsvsb[1].Value.ToString()) && Convert.ToInt64(tsvsb[1].Value) != 0)
                        {
                            pccb = pccb + tsvsb[1].Value;
                            pcb = tsvsb[1].Value;
                        }
                        else
                        {
                            pccb = pccb + 0;
                            pcb = 0;
                        }

                        // baseline1 costs
                        tsvsb1 = activeProj.ProjectSummaryTask.TimeScaleData(
                            StartDate: tsv.StartDate,
                            EndDate: tsv.EndDate,
                            Type: MSProject.PjTaskTimescaledData.pjTaskTimescaledBaseline1Cost,
                            TimeScaleUnit: MSProject.PjTimescaleUnit.pjTimescaleMonths, Count: 1);

                        if (!string.IsNullOrEmpty(tsvsb1[1].Value.ToString()) && Convert.ToInt64(tsvsb1[1].Value) != 0)
                        {
                            pccb1 = pccb1 + tsvsb1[1].Value;
                            pcb1 = tsvsb1[1].Value;
                        }
                        else
                        {
                            pccb1 = pccb1 + 0;
                            pcb1 = 0;
                        }

                        // baseline2 costs
                        tsvsb2 = activeProj.ProjectSummaryTask.TimeScaleData(
                            StartDate: tsv.StartDate,
                            EndDate: tsv.EndDate,
                            Type: MSProject.PjTaskTimescaledData.pjTaskTimescaledBaseline2Cost,
                            TimeScaleUnit: MSProject.PjTimescaleUnit.pjTimescaleMonths, Count: 1);

                        if (!string.IsNullOrEmpty(tsvsb2[1].Value.ToString()) && Convert.ToInt64(tsvsb2[1].Value) != 0)
                        {
                            pccb2 = pccb2 + tsvsb2[1].Value;
                            pcb2 = tsvsb2[1].Value;
                        }
                        else
                        {
                            pccb2 = pccb2 + 0;
                            pcb2 = 0;
                        }

                        // baseline3 costs
                        tsvsb3 = activeProj.ProjectSummaryTask.TimeScaleData(
                            StartDate: tsv.StartDate,
                            EndDate: tsv.EndDate,
                            Type: MSProject.PjTaskTimescaledData.pjTaskTimescaledBaseline3Cost,
                            TimeScaleUnit: MSProject.PjTimescaleUnit.pjTimescaleMonths, Count: 1);

                        if (!string.IsNullOrEmpty(tsvsb3[1].Value.ToString()) && Convert.ToInt64(tsvsb3[1].Value) != 0)
                        {
                            pccb3 = pccb3 + tsvsb3[1].Value;
                            pcb3 = tsvsb3[1].Value;
                        }
                        else
                        {
                            pccb3 = pccb3 + 0;
                            pcb3 = 0;
                        }

                        // write values to spreadsheet
                        xlWorksheet.Cells[row, 1].Value = instanceName;
                        xlWorksheet.Cells[row, 2].Value = startYear;
                        xlWorksheet.Cells[row, 3].Value = startMonthStr;
                        xlWorksheet.Cells[row, 4].Value = fisYear;
                        xlWorksheet.Cells[row, 5].Value = fisMonth;
                        xlWorksheet.Cells[row, 6].Value = pc;
                        xlWorksheet.Cells[row, 7].Value = pcc;
                        xlWorksheet.Cells[row, 8].Value = pca;
                        xlWorksheet.Cells[row, 9].Value = pcca;
                        xlWorksheet.Cells[row, 10].Value = pcb;
                        xlWorksheet.Cells[row, 11].Value = pccb;
                        xlWorksheet.Cells[row, 12].Value = pcb1;
                        xlWorksheet.Cells[row, 13].Value = pccb1;
                        xlWorksheet.Cells[row, 14].Value = pcb2;
                        xlWorksheet.Cells[row, 15].Value = pccb2;
                        xlWorksheet.Cells[row, 16].Value = pcb3;
                        xlWorksheet.Cells[row, 17].Value = pccb3;
                        row = row + 1; // increment the row
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
                MessageBox.Show(ERROR_MESG, Globals.ThisAddIn.Application.Name,
                                 MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        public void XlExportTimePhasedWork(string instanceName, string worksheetName)
        {
            // Remy Dawson 2015
            // Extracts monthly time phased assignment data from a schedule file and writes it to a spreadsheet.
            // The active worksheet in the active workbook receives the data.
            // Data from additional projects will be appended.

            long row;
            MSProject.Project activeProj;
            MSProject.TimeScaleValues tsvs; // work
            MSProject.TimeScaleValues tsvsc; // cost - not implemented yet
            MSProject.Resources resources;
            MSProject.Assignments assignments;
            string s1 = null;
            string s2 = null;
            string s3 = null;
            MSProject.Task tsk;
            string outlineCode;
            int startYear, fisYear, startMonth, fisMonth;
            string startMonthStr;
            FiscalDate fisDate;

            initializeExcelEnv(worksheetName);

            try
            {
                // extracts data from the active project
                activeProj = Globals.ThisAddIn.Application.ActiveProject;

                // if there isn't anything that looks like a header row, add it
                //if (getExportType(xlWorksheet) != EXPORT_TIME_PHASED_WORK)
                if(xlWorksheet.Cells[1, 1].Value != "File Name")
                {
                    //setExportType(xlWorksheet, EXPORT_TIME_PHASED_WORK);
                    xlWorksheet.Rows[1].Insert();
                    xlWorksheet.Cells[1, 1].Value = "File Name";
                    xlWorksheet.Cells[1, 2].Value = "Level 1";
                    xlWorksheet.Cells[1, 3].Value = "Level 2";
                    xlWorksheet.Cells[1, 4].Value = "Level 3";
                    xlWorksheet.Cells[1, 5].Value = "TaskSummaryName";
                    xlWorksheet.Cells[1, 6].Value = "WBS";
                    xlWorksheet.Cells[1, 7].Value = "TaskName";
                    xlWorksheet.Cells[1, 8].Value = "Group";
                    xlWorksheet.Cells[1, 9].Value = "Skill";
                    xlWorksheet.Cells[1, 10].Value = "Resource";
                    xlWorksheet.Cells[1, 11].Value = "Year";
                    xlWorksheet.Cells[1, 12].Value = "Month";
                    xlWorksheet.Cells[1, 13].Value = "Fiscal Year";
                    xlWorksheet.Cells[1, 14].Value = "Fiscal Month";
                    xlWorksheet.Cells[1, 15].Value = "Work (hours)";
                    xlWorksheet.Cells[1, 16].Value = "Work";
                    xlWorksheet.Cells[1, 17].Value = "FTE (month)";
                    xlWorksheet.Cells[1, 18].Value = "FTE (year)";
                    xlWorksheet.Cells[1, 19].Value = "Available Units";
                }

                xlRange = xlWorksheet.UsedRange;
                row = xlRange.Rows.Count + 1;

                // Iterate through each resource and their assignments
                resources = activeProj.Resources;
                foreach (MSProject.Resource resource in resources)
                {
                    if (resource.Type == MSProject.PjResourceTypes.pjResourceTypeWork)
                    {
                        outlineCode = resource.OutlineCode1; // resource OutlineCode1 contains the skill
                        assignments = resource.Assignments;
                        foreach (MSProject.Assignment ass in assignments)
                        {
                            tsvs = ass.TimeScaleData(StartDate: activeProj.ProjectStart, EndDate: activeProj.ProjectFinish,
                                Type: MSProject.PjAssignmentTimescaledData.pjAssignmentTimescaledWork, 
                                TimeScaleUnit: MSProject.PjTimescaleUnit.pjTimescaleMonths, Count: 1);
                            tsvsc = ass.TimeScaleData(StartDate: activeProj.ProjectStart, EndDate: activeProj.ProjectFinish,
                                Type: MSProject.PjAssignmentTimescaledData.pjAssignmentTimescaledCost, 
                                TimeScaleUnit: MSProject.PjTimescaleUnit.pjTimescaleMonths, Count: 1);
                            
                            tsk = activeProj.Tasks.UniqueID[ass.TaskUniqueID];
                            // Iterate up to the level 1 summary task to capture outline task names
                            while (tsk.OutlineLevel != 1)
                            {
                                if (tsk.OutlineLevel == 3)
                                {
                                    s3 = tsk.Name;
                                }
                                if (tsk.OutlineLevel == 2)
                                {
                                    s2 = tsk.Name;
                                }
                                tsk = tsk.OutlineParent;
                            }
                            s1 = tsk.Name;

                            // Iterate the TimeScaledValues.
                            // Note that conversion factors are approximate.
                            // 480 -> not all months have 20 days * 24 hours
                            foreach (MSProject.TimeScaleValue tsv in tsvs)
                            {
                                if (!string.IsNullOrEmpty(tsv.Value.ToString()) && Convert.ToInt64(tsv.Value) != 0)
                                {
                                    startYear = Convert.ToInt32(String.Format("{0:yyyy}", tsv.StartDate));
                                    startMonthStr = String.Format("{0:MMM}", tsv.StartDate);
                                    startMonth = Convert.ToInt32(String.Format("{0:MM}", tsv.StartDate));

                                    fisDate = new FiscalDate(startYear, startMonth);
                                    fisYear = fisDate.GetFiscalYear();
                                    fisMonth = fisDate.GetFiscalMonth();
                                    
                                    // write values to spreadsheet
                                    xlWorksheet.Cells[row, 1].Value = instanceName;
                                    xlWorksheet.Cells[row, 2].Value = s1;
                                    xlWorksheet.Cells[row, 3].Value = s2;
                                    xlWorksheet.Cells[row, 4].Value = s3;
                                    xlWorksheet.Cells[row, 5].Value = ass.TaskSummaryName;
                                    xlWorksheet.Cells[row, 6].Value = ass.WBS;
                                    xlWorksheet.Cells[row, 7].Value = ass.TaskName;
                                    xlWorksheet.Cells[row, 8].Value = resource.Group;
                                    xlWorksheet.Cells[row, 9].Value = outlineCode;
                                    xlWorksheet.Cells[row, 10].Value = resource.Name;
                                    xlWorksheet.Cells[row, 11].Value = startYear;
                                    xlWorksheet.Cells[row, 12].Value = startMonthStr;
                                    xlWorksheet.Cells[row, 13].Value = fisYear;
                                    xlWorksheet.Cells[row, 14].Value = fisMonth;
                                    xlWorksheet.Cells[row, 15].Value = tsv.Value;
                                    xlWorksheet.Cells[row, 16].Value = Convert.ToInt64(tsv.Value) / 480;
                                    xlWorksheet.Cells[row, 17].Value = Convert.ToInt64(tsv.Value) / (480 * 20);
                                    xlWorksheet.Cells[row, 18].Value = Convert.ToInt64(tsv.Value) / 115200;
                                    xlWorksheet.Cells[row, 19].Value = resource.Availabilities[1].AvailableUnit;
                                    row = row + 1; // increment the row
                                }
                            }
                        }
                    }
                }
                xlRange = xlWorksheet.UsedRange;
                xlRange.Name = "mspExport"; // name the final range
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(ERROR_MESG, Globals.ThisAddIn.Application.Name,
                                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Console.WriteLine(e.ToString());
            }
        }

        private void initializeExcelEnv(string worksheetName)
        {
            try
            {
                xlApp = (Excel.Application)Marshal.GetActiveObject("Excel.Application");
                xlApp.Visible = true;
                xlWorkbook = xlApp.ActiveWorkbook;
                xlWorksheet = getSelectedWorkSheet(xlWorkbook, worksheetName);
            }
            catch (Exception e)
            {
                xlError();
                Console.WriteLine(e.ToString());
            }
        }

        private string getExportType(Excel.Worksheet worksheet)
        {
            foreach(Excel.CustomProperty prop in worksheet.CustomProperties)
            {
                if(prop.Name == EXPORT_TYPE_NAME)
                {
                    return prop.Value;
                }
            }

            return null;            
        }

        private void setExportType(Excel.Worksheet worksheet, string value)
        {
            foreach(Excel.CustomProperty prop in worksheet.CustomProperties)
            {
                if(prop.Name == EXPORT_TYPE_NAME)
                {
                    prop.Value = value;
                    return;
                }
            }

            worksheet.CustomProperties.Add(Name: EXPORT_TYPE_NAME, Value: value);
        }

        private Excel.Worksheet getSelectedWorkSheet(Excel.Workbook xlWorkbook, string worksheetName)
        {
            foreach(Excel.Worksheet worksheet in xlWorkbook.Worksheets)
            {
                if(worksheet.Name == worksheetName)
                {
                    return worksheet;
                }
            }

            return null;
        }

        private void xlError()
        {
            MessageBox.Show("Could not open the worksheet, try closing and reopening the file.", "Error");
            throw new Exception();
        }
    }
}
