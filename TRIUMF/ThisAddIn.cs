﻿using MSProject = Microsoft.Office.Interop.MSProject;
using Office = Microsoft.Office.Core;

namespace TRIUMF
{
    public partial class ThisAddIn
    {
        protected override Office.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new Ribbon1();
        }

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            if (Properties.Settings.Default.ThreePointDuration)
                EnableThreePointDuration();
        }

        public void ShowColumns()
        {
            this.Application.TableEditEx(Name: "&Entry", TaskTable: true, NewName: "", FieldName: "Duration", 
                NewFieldName: "Duration", Title: "Calculated Duration", Width: 11, Align: 0, ShowInMenu: true, LockFirstColumn: true, 
                DateFormat: 255, RowHeight: 1, ColumnPosition: 3, AlignTitle: 0);
            this.Application.TableEditEx(Name: "&Entry", TaskTable: true, NewName: "", FieldName: "Duration1",
                NewFieldName: "Duration1", Title: "Optimistic Duration", Width: 11, Align: 0, ShowInMenu: true, LockFirstColumn: true,
                DateFormat: 255, RowHeight: 1, ColumnPosition: 4, AlignTitle: 0);
            this.Application.TableEditEx(Name: "&Entry", TaskTable: true, NewName: "", FieldName: "Duration2",
                NewFieldName: "Duration2", Title: "Likely Duration", Width: 11, Align: 0, ShowInMenu: true, LockFirstColumn: true,
                DateFormat: 255, RowHeight: 1, ColumnPosition: 5, AlignTitle: 0);
            this.Application.TableEditEx(Name: "&Entry", TaskTable: true, NewName: "", FieldName: "Duration3",
                NewFieldName: "Duration3", Title: "Pessimistic Duration", Width: 11, Align: 0, ShowInMenu: true, LockFirstColumn: true,
                DateFormat: 255, RowHeight: 1, ColumnPosition: 6, AlignTitle: 0);
            this.Application.TableApply(Name: "&Entry");
        }

        public void ToggleThreePointDuration()
        {
            if (Properties.Settings.Default.ThreePointDuration)
                EnableThreePointDuration();
            else
                DisableThreePointDuration();
        }

        private void EnableThreePointDuration()
        {
            bool f = false;

            this.Application.ProjectBeforeTaskChange += Application_ProjectBeforeTaskChange;

            foreach (MSProject.Task task in this.Application.ActiveProject.Tasks)
            {
                //This is really messy but we need to give the handler dummy parameters
                Application_ProjectBeforeTaskChange(task, MSProject.PjField.pjImportResource, null, ref f);
            }
        }

        private void Application_ProjectBeforeTaskChange(MSProject.Task task, MSProject.PjField field, object newVal, ref bool cancel)
        {
            if(!cancel && Properties.Settings.Default.ThreePointDuration)
            {
                ThreePointDuration tpd = ThreePointDuration.createFromTask(task, field, newVal);
                task.SetField(Globals.ThisAddIn.Application.FieldNameToFieldConstant("Duration"), tpd.calcDuration() + " days");
            }
        }
        
        private void DisableThreePointDuration()
        {
            this.Application.ProjectBeforeTaskChange -= Application_ProjectBeforeTaskChange;       
        }



        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
