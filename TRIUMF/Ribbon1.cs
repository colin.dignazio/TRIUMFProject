﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using Office = Microsoft.Office.Core;

namespace TRIUMF
{
    [ComVisible(true)]
    public class Ribbon1 : Office.IRibbonExtensibility
    {
        public Ribbon1()
        {
        }

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("TRIUMF.Ribbon1.xml");
        }

        #endregion

        #region Ribbon Callbacks
        //Create callback methods here. For more information about adding callback methods, visit http://go.microsoft.com/fwlink/?LinkID=271226

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            ribbonUI.Invalidate();
        }

        public void adxRibbonButton1_OnClick(Office.IRibbonControl control, bool isPressed)
        {
            Properties.Settings.Default.ThreePointDuration = isPressed ? true : false;
            Properties.Settings.Default.Save();
            Globals.ThisAddIn.ToggleThreePointDuration();
        }

        public void adxRibbonButton2_OnClick(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.ShowColumns();
        }

        public bool get_Pressed(Office.IRibbonControl control)
        {
            if (Properties.Settings.Default.ThreePointDuration)
                return true;
            else
                return false;
        }

        public void xlExportSCurveData(Office.IRibbonControl control)
        {
            try
            {
                ExportForm exportForm = new ExportForm("XlExportSCurveData");
                exportForm.Show();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void xlExportTimePhasedWork(Office.IRibbonControl control)
        {
            try
            {
                ExportForm exportForm = new ExportForm("XlExportTimePhasedWork");
                exportForm.Show();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        public void showSettings(Office.IRibbonControl control)
        {
            Settings settings = new Settings();
            settings.OpenSettingsDialog();
        }

        #endregion

        #region Helpers

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
