﻿using System;
using System.Windows.Forms;

namespace TRIUMF
{
    class Settings
    {
        private readonly string TASK_ENTRY = "Task Entry";
        public readonly string MSG_NO_PROJECT_OPEN = "You do not have a project open. Open a project, and then run the macro again.";
        private readonly string MSG_NO_TASKS = "There are no tasks in the current project. Try again after the project has one or more tasks.";
        private string MSG_UNDEFINED = "An unexpected error occurred:";
        private string MSG_PROJECT_NOT_SAVED = "The current project is not saved. Save the project, and then run the macro again.";
        
        public void OpenSettingsDialog()
        {
            SettingsForm settingsForm = new SettingsForm();

            Globals.ThisAddIn.Application.OpenUndoTransaction("Apply TRIUMF Tools|Options defaults");

            if(Globals.ThisAddIn.Application.ActiveProject != null)
            {
                settingsForm.Show();
            }
            else
            {
                System.Windows.Forms.MessageBox.Show(MSG_NO_PROJECT_OPEN, Globals.ThisAddIn.Application.Name,
                                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            Globals.ThisAddIn.Application.CloseUndoTransaction();
        }
    }
}
