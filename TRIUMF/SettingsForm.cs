﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MSProject = Microsoft.Office.Interop.MSProject;

namespace TRIUMF
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            settingsTextBox.Text = "Clicking the Set Defaults button will change all Tools|Options settings for " + 
                                Globals.ThisAddIn.Application.ActiveProject.Name + " to TRIUMF defaults.";
        }

        private void setDefaults_Click(object sender, EventArgs e)
        {
            stdToolsOptions();
            this.Close();
        }

        private void close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void stdToolsOptions()
        {
            // TRIUMF standard Tools|Options settings
            Globals.ThisAddIn.Application.OptionsGeneralEx(AutoAddResources: true, StandardRate: "$ 0/h", OvertimeRate: "$ 0/h", 
                               SetDefaults: true, AutoFilter: true, RecentFilesMaximum: 9, MaxUndoRecords: 20);
            Globals.ThisAddIn.Application.OptionsCalendar(StartYearIn: 1, StartTime: "8", FinishTime: "17", HoursPerDay: 8, HoursPerWeek: 40, 
                    SetDefaults: true, StartWeekOn: 2, UseFYStartYear: true, DaysPerMonth: 20);
            Globals.ThisAddIn.Application.OptionsViewEx(ProjectSummary: true, SymbolPlacement: 2, CurrencyDigits: 0, ProjectCurrency: "CAD", 
                        Use3DLook: false);
            Globals.ThisAddIn.Application.OptionsEdit(MinuteLabelDisplay: 0, HourLabelDisplay: 0, DayLabelDisplay: 0, WeekLabelDisplay: 0, 
                        YearLabelDisplay: 0, SpaceBeforeTimeLabel: true, SetDefaults: true, MonthLabelDisplay: 0, HyperlinkColor: 5, FollowedHyperlinkColor: 12, UnderlineHyperlinks: true);
            Globals.ThisAddIn.Application.OptionsSchedule(TaskType: MSProject.PjTaskFixedType.pjFixedDuration, DurationUnits: MSProject.PjUnit.pjDay, WorkUnits: MSProject.PjUnit.pjDay, 
                       AssignmentUnits: MSProject.PjAssignmentUnits.pjDecimalAssignmentUnits, AutoLink: false, EffortDriven: false, StartOnCurrentDate: false);
            Globals.ThisAddIn.Application.OptionsInterfaceEx(DisplayProjectGuide: false, ProjectGuideFunctionalLayoutPage: "gbui://mainpage.htm", 
                      ProjectGuideContent: "gbui://gbui.xml", SetAsDefaults: true);
        }
    }
}
