﻿using System;
using MSProject = Microsoft.Office.Interop.MSProject;

namespace TRIUMF
{
    public class ThreePointDuration
    {
        private long oDur, lDur, pDur;

        public ThreePointDuration(long d1, long d2, long d3)
        {
            oDur = d1;
            lDur = d2;
            pDur = d3;
        }

        public static ThreePointDuration createFromTask(MSProject.Task task, MSProject.PjField field, object newVal)
        {
            long d1, d2, d3;
            long newDuration = -1;

            if (newVal != null)
            {
                newDuration = Convert.ToInt64(newVal.ToString().Substring(0, 1)) * 480;
            }
            
            switch(field)
            {
                case MSProject.PjField.pjTaskDuration1:
                    d1 = newDuration;
                    d2 = Convert.ToInt64(task.Duration2);
                    d3 = Convert.ToInt64(task.Duration3);
                    break;
                case MSProject.PjField.pjTaskDuration2:
                    d1 = Convert.ToInt64(task.Duration1);
                    d2 = newDuration;
                    d3 = Convert.ToInt64(task.Duration3);
                    break;
                case MSProject.PjField.pjTaskDuration3:
                    d1 = Convert.ToInt64(task.Duration1);
                    d2 = Convert.ToInt64(task.Duration2);
                    d3 = newDuration;
                    break;
                default:
                    d1 = Convert.ToInt64(task.Duration1);
                    d2 = Convert.ToInt64(task.Duration2);
                    d3 = Convert.ToInt64(task.Duration3);
                    break;
            }

            return new ThreePointDuration(d1, d2, d3);
        }

        public long calcDuration()
        {
            long duration = (oDur + (4 * lDur) + pDur) / 6;
            double res = Math.Round((duration / 480.0));
            duration = (long)res;

            return duration;
        }
    }

}