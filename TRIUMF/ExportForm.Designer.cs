﻿namespace TRIUMF
{
    partial class ExportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.exportInput = new System.Windows.Forms.TextBox();
            this.submit = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.exportCombo = new System.Windows.Forms.ComboBox();
            this.exportTextBox = new System.Windows.Forms.RichTextBox();
            this.browse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // exportInput
            // 
            this.exportInput.Location = new System.Drawing.Point(12, 30);
            this.exportInput.Name = "exportInput";
            this.exportInput.Size = new System.Drawing.Size(260, 20);
            this.exportInput.TabIndex = 0;
            this.exportInput.KeyDown += new System.Windows.Forms.KeyEventHandler(this.instanceInput_KeyDown);
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(64, 90);
            this.submit.Name = "submit";
            this.submit.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.submit.Size = new System.Drawing.Size(75, 23);
            this.submit.TabIndex = 2;
            this.submit.Text = "Submit";
            this.submit.UseVisualStyleBackColor = true;
            this.submit.Click += new System.EventHandler(this.submit_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(145, 90);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(75, 23);
            this.cancel.TabIndex = 3;
            this.cancel.Text = "Cancel";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // exportCombo
            // 
            this.exportCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.exportCombo.FormattingEnabled = true;
            this.exportCombo.Location = new System.Drawing.Point(12, 56);
            this.exportCombo.Name = "exportCombo";
            this.exportCombo.Size = new System.Drawing.Size(179, 21);
            this.exportCombo.TabIndex = 4;
            // 
            // exportTextBox
            // 
            this.exportTextBox.BackColor = System.Drawing.SystemColors.Control;
            this.exportTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.exportTextBox.Location = new System.Drawing.Point(12, 9);
            this.exportTextBox.Name = "exportTextBox";
            this.exportTextBox.ReadOnly = true;
            this.exportTextBox.Size = new System.Drawing.Size(260, 21);
            this.exportTextBox.TabIndex = 5;
            this.exportTextBox.Text = "";
            // 
            // browse
            // 
            this.browse.Location = new System.Drawing.Point(197, 55);
            this.browse.Name = "browse";
            this.browse.Size = new System.Drawing.Size(75, 23);
            this.browse.TabIndex = 6;
            this.browse.Text = "Browse";
            this.browse.UseVisualStyleBackColor = true;
            this.browse.Click += new System.EventHandler(this.browse_Click);
            // 
            // ExportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 121);
            this.Controls.Add(this.browse);
            this.Controls.Add(this.exportTextBox);
            this.Controls.Add(this.exportCombo);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.exportInput);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExportForm";
            this.ShowIcon = false;
            this.Text = "Excel Export";
            this.Load += new System.EventHandler(this.ExportForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox exportInput;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Button cancel;
        private System.Windows.Forms.ComboBox exportCombo;
        private System.Windows.Forms.RichTextBox exportTextBox;
        private System.Windows.Forms.Button browse;
    }
}